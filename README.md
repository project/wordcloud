# Wordcloud

## Table of contents

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## Introduction

A Wordle ike Wordcloud implementation to generate wordclouds based on the
content of the site.


## Requirements

* D3 Wordcloud
  https://github.com/wvengen/d3-wordcloud

  - Download the library from
    https://github.com/wvengen/d3-wordcloud/archive/refs/heads/master.zip
  - Extract the library to
    `/libraries/d3-wordcloud/`

## Installation

Install the Wordcloud as you would install any other contributed Drupal module. 
See [Installing modules on d.o](https://www.drupal.org/node/1897420) for
further information.

## Configuration

* Got to [Administration > Structure > Block layout](/admin/structure/block)
* Place a Wordcloud block in any region you see fit

## Maintainers

- Stefan Auditor - [sanduhrs](https://www.drupal.org/u/sanduhrs)
