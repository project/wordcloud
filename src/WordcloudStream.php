<?php

declare(strict_types = 1);

namespace Drupal\wordcloud;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Wordcloud Stream class.
 */
final class WordcloudStream {

  /**
   * The entity type.
   *
   * @var string
   *   The entity type identifier string.
   */
  protected string $entityType;

  /**
   * The entity type bundle.
   *
   * @var string
   *   The entity type bundle identifier string.
   */
  protected string $entityBundle;

  /**
   * The limit.
   *
   * @var int
   *   The query limit integer.
   */
  protected int $limit;

  /**
   * Constructs a Words object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly LanguageManagerInterface $languageManager,
    private readonly Connection $connection,
    private readonly CacheBackendInterface $cache,
  ) {
    $this->limit = 0;
  }

  /**
   * Sets the entity type.
   *
   * @param string $identifier
   *   The entity type identifier.
   */
  public function setEntityType(string $identifier): static {
    $this->entityType = $identifier;
    return $this;
  }

  /**
   * Sets the entity type bundle.
   *
   * @param string $identifier
   *   The entity type bundle identifier.
   */
  public function setEntityBundle(string $identifier): static {
    $this->entityBundle = $identifier;
    return $this;
  }

  /**
   * Sets the language.
   *
   * @param string $langcode
   *   The langcode string.
   */
  public function setLanguage(string $langcode): static {
    $this->langcode = $langcode;
    return $this;
  }

  /**
   * Sets the query limit.
   *
   * @param int $limit
   *   The limit integer.
   */
  public function setLimit(int $limit): static {
    $this->limit = $limit;
    return $this;
  }

  /**
   * Gets the words stream.
   */
  public function getWords() {
    $cid = implode(':', [
      'wordcloud',
      $this->entityType,
      $this->entityBundle,
      $this->limit,
    ]);
    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }

    $query = $this->connection->select('taxonomy_term_data', 'td');
    $query->addExpression('COUNT(td.tid)', 'count');
    // $query->addExpression('td.name', 'text');
    $query->fields('tfd', ['name']);
    $query->fields('td', ['tid', 'vid']);
    $query->addExpression('MIN(tn.nid)', 'nid');
    $query->join('taxonomy_index', 'tn', 'td.tid = tn.tid');
    $query->join('node_field_data', 'n', 'tn.nid = n.nid');
    $query->join('taxonomy_term_field_data', 'tfd', 'tfd.tid = tn.tid');
    // $query->condition('n.langcode', $language->getId());
    $query->condition('td.vid', $this->entityBundle, 'IN');
    $query->condition('n.status', 1);
    $query->condition('tfd.status', 1);
    $query->groupBy('td.tid')->groupBy('td.vid')->groupBy('tfd.name');
    // $query->groupBy('tfd.description__value');
    $query->having('COUNT(td.tid)>0');
    $query->orderBy('count', 'DESC');

    if ($this->limit > 0) {
      $query->range(0, $this->limit);
    }

    $result = $query->execute()->fetchAll();
    $rows = [];
    foreach ($result as $row) {
      $rows[] = [
        'text' => $row->name,
        'size' => $row->count,
        // @todo Get canonical entity link.
        'href' => '',
      ];
    }
    $this->cache->set($cid, $rows, CacheBackendInterface::CACHE_PERMANENT, [
      $this->entityType . '_list',
    ]);
    return $rows;
  }

}
