<?php

declare(strict_types = 1);

namespace Drupal\wordcloud\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\wordcloud\Palettes;
use Drupal\wordcloud\WordcloudStream;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a wordcloud block.
 *
 * @Block(
 *   id = "wordcloud_wordcloud",
 *   admin_label = @Translation("Wordcloud"),
 *   category = @Translation("Content display"),
 * )
 */
final class WordcloudBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly WordcloudStream $stream,
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('wordcloud.stream'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'entity_type' => 'taxonomy_term',
      'entity_bundle' => '',
      'width' => 300,
      'height' => 300,
      'scale' => 'linear',
      'transition_duration' => 0,
      'fill' => 'drupal',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $vocabulary_options = [];
    foreach ($vocabularies as $vocabulary) {
      $vocabulary_options[$vocabulary->get('vid')] = $vocabulary->get('name');
    }
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#required' => TRUE,
      '#options' => [
        'taxonomy_term' => $this->t('Taxonomy term'),
      ],
      '#default_value' => $this->configuration['entity_type'],
    ];
    $form['entity_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity bundle'),
      '#required' => TRUE,
      '#options' => $vocabulary_options,
      '#default_value' => $this->configuration['entity_bundle'],
    ];
    $form['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['width'],
    ];
    $form['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['height'],
    ];
    $form['scale'] = [
      '#type' => 'radios',
      '#title' => $this->t('Scale'),
      '#required' => TRUE,
      '#options' => [
        'log' => $this->t('log n'),
        'sqrt' => $this->t('√n'),
        'linear' => $this->t('n'),
      ],
      '#default_value' => $this->configuration['scale'],
    ];
    $form['transition_duration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Transition duration'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['transition_duration'],
    ];
    $form['fill'] = [
      '#type' => 'select',
      '#title' => $this->t('Fill'),
      '#required' => TRUE,
      '#options' => Palettes::getOptionsArray('colors'),
      '#default_value' => $this->configuration['fill'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['entity_type'] = $form_state->getValue('entity_type');
    $this->configuration['entity_bundle'] = $form_state->getValue('entity_bundle');
    $this->configuration['width'] = $form_state->getValue('width');
    $this->configuration['height'] = $form_state->getValue('height');
    $this->configuration['scale'] = $form_state->getValue('scale');
    $this->configuration['fill'] = $form_state->getValue('fill');
    $this->configuration['transition_duration'] = $form_state->getValue('transition_duration');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $words = $this->stream
      ->setEntityType($this->configuration['entity_type'])
      ->setEntityBundle($this->configuration['entity_bundle'])
      ->setLimit(100)
      ->getWords();
    $id = Html::getUniqueId('wordcloud-block');
    $build = [
      '#theme' => 'wordcloud',
      '#id' => $id,
      '#width' => $this->configuration['width'],
      '#height' => $this->configuration['height'],
      '#transition_duration' => $this->configuration['transition_duration'],
      '#scale' => $this->configuration['scale'],
      '#fill' => $this->configuration['fill'],
      '#entity_type' => $this->configuration['entity_type'],
      '#entity_bundle' => $this->configuration['entity_bundle'],
      '#words' => $words,
      '#attached' => [
        'library' => [
          'wordcloud/wordcloud',
        ],
        'drupalSettings' => [
          'wordcloud' => [
            $id => [
              'id' => $id,
              'width' => $this->configuration['width'],
              'height' => $this->configuration['height'],
              'transition_duration' => $this->configuration['transition_duration'],
              'scale' => $this->configuration['scale'],
              'fill' => explode(',', $this->configuration['fill']),
              '#e' => $this->configuration['entity_type'],
              '#dle' => $this->configuration['entity_bundle'],
              'words' => $words,
            ],
          ],
        ],
      ],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), [
      'taxonomy_term_list',
    ]);
  }

}
