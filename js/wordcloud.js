/**
 * Wordcloud behaviors.
 */
(function ($, Drupal, once) {
  Drupal.behaviors.wordcloud = {
    attach: function (context, settings) {
      once('wordcloud', '.block-wordcloud-wordcloud .wordcloud', context)
      .forEach((element) => {
        let config = settings.wordcloud[element.id];
        console.log(config);

        d3.wordcloud()
          .selector('#' + config.id)
          .size([config.width, config.height])
          .scale(config.scale)
          .words(config.words)
          .fill(d3.scale.ordinal().range(config.fill))
          .transitionDuration(element.dataset.transitionDuration)
          .onwordclick(function (d, i) {
            if (d.href) { window.location = d.href; }
          })
          .start();
      });
  }
};
})(jQuery, Drupal, once);
